pec1---un-juego-de-carreras
# PEC 1 - Un juego de carreras - Ghost Race
Primera entrega de la asignatura de programación en Unity 3D. En ella creamos un juego de carreras cronometrado donde podemos competir contra nuestro mejor resultado.

## Descripción

Para el desarrollo de este proyecto hemos utilizado algunas de las herramientas más comunes en la mayoría de juegos 3D creados en Unity. Para la creación del mundo en el que se desarrolla la partida hemos utilizado el editor de terreno de Unity. El circuito ha sido creado utilizando EasyRoads3D y el coche y sus controles son parte de los Standard Assets de Unity. Aprender a utilizar estas herramientas nos ha permitido crear el juego en un breve período de tiempo, centrándonos más en la lógica de la partida.

La carrera transcurre a lo largo de tres vueltas. El jugador puede ver sus tiempos en cada vuelta y el tiempo total al final de la partida. Además, si consigue registrar un nuevo record, su recorrido será guardado y reproducido por un coche fantasma contra el que poder competir.

## Cómo jugar
- Para acelerar el vehículo tanto hacia adelante como hacia atrás se utilizan las teclas **W** y **S** respectivamente o las teclas de flecha **ARRIBA** y **ABAJO**.
- Para desplazarse a izquierda y derecha el jugador debe utilizar las teclas **A** y **D** del teclado respectivamente, o las teclas de flecha **IZQUIERDA** y **DERECHA**.
- Para utilizar el freno de mano se utiliza la tecla **ESPACIO**

Al principio de la partida tenemos una cuenta atrás que nos indica cuando empezar la carrera. Tras la señal de salida, el tiempo de cada vuelta empieza a correr y queda registrado en el HUD del jugador al superar cad vuelta. A lo largo del recorrido de la pista hay dispuestos unos puntos de control 'Checkpoints' por los que el jugador debe pasar. Hasta no superar el Checkpoint actual no se activará el siguiente. Cuando se hayan completado las tres vueltas, la partida terminará y el jugador sabrá si ha superado el record actual o no con un mensaje en la pantalla.

En [este video](https://youtu.be/PG0OGvBjZL4) se puede ver una partida de prueba compitiendo contra el fantasma de la partida anterior.

## Clases principales

### CarPath

Se encarga de guardar el recorrido del usuario: su posición, rotación y el instante de tiempo en el que pasó por cada punto a lo largo de la carrera.

### CarPathLoader

Se encarga de cargar los datos del record actual, si los hubiera.

### CarPathRecorder

Se encarga de guardar los datos del recorrido actual cuando el usuario ha superado el record.

### CarReplay

Tras cada partida, el jugador puede ver una repetición de su recorrido. Esta clase gestiona el movimiento del vehículo durante esa repetición.

### Checkpoint

Contiene la lógica que se activa cuando el jugador pasa por un checkpoint. Esta clase avisa al RaceManager para que actualice el status de la carrera.

### GhostController

Controla el movimiento del coche fantasma.

### LapCount

Tiene una lista de las vueltas y guarda los tiempos para cada una de ellas.

### MenuController

Controla la lógica para iniciar la partida o salir del juego.

### RaceManager

Es la clase principal del juego y gestiona el flujo de la partida. Se encarga de activar y desactivar los diferentes elementos del juego, como por ejemplo el coche fantasma. Controla el momento en el que el jugador termina la partida y activa el modo de repetición.

### ReplayController

Se encarga de ejecutar la lógica para mostrar la repetición de la carrera.

### TerrainCheck

Controla si el coche se ha salido de la pista, en cuyo caso, hace que el movimiento del vehículo sea más lento.

### TimeCount

Se encarga de registrar el tiempo actual, fraccionándolo en minutos, segundos y milisegundos, para pasarle los resultados a la clase LapCount.

### UIController

Ofrece métodos para actualizar los diferentes campos de la UI durante la partida: tiempo de las vueltas, vueltas restantes, etc.