﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarPathLoader {
	private TextAsset bestRaceData;
	private CarPath carPath = null;

	public CarPathLoader(){
		bestRaceData = Resources.Load("BestRace") as TextAsset;
		if(bestRaceData != null){
			carPath = JsonUtility.FromJson<CarPath>(bestRaceData.ToString());
		}
	}

	public CarPath getCarPath(){
		return carPath;
	}
}
