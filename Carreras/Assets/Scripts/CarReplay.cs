﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarReplay : MonoBehaviour
{
  public Transform pointA, pointB;
  public float currentTime;

  public List<CarPoint> points;
  public int currentSegmentIndex = 0;

  private CarPath carPath;

  private void MakeUpdates()
  {
    UpdateSegment();
    Move();
  }

  public void StartReplay(CarPath cp)
  {
    carPath = cp;
    RestartValues();
    points = carPath.GetCarPoints();
    if (points.Count >= 2) PrepareFirstSegment();

    InvokeRepeating("MakeUpdates", 0f, 0.1f);
  }

  private void RestartValues()
  {
    pointA.position = Vector3.zero;
    pointA.rotation = Quaternion.identity;
    pointB.position = Vector3.zero;
    pointB.rotation = Quaternion.identity;
    points = new List<CarPoint>();
  }

  private void PrepareFirstSegment()
  {
    pointA.position = points[currentSegmentIndex].GetPosition();
    pointA.rotation = points[currentSegmentIndex].GetRotation();
    currentSegmentIndex++;
    pointB.position = points[currentSegmentIndex].GetPosition();
    pointB.rotation = points[currentSegmentIndex].GetRotation();
    currentSegmentIndex++;
  }

  private void UpdateSegment()
  {
    // Point A data
    pointA.position = pointB.position;
    pointA.rotation = pointB.rotation;
    // Point B data
    if (points.Count > currentSegmentIndex)
    {
      pointB.position = points[currentSegmentIndex].GetPosition();
      pointB.rotation = points[currentSegmentIndex].GetRotation();
      currentSegmentIndex++;
    }
  }

  private void Move()
  {
    float currentTime = Time.time % 0.1f;
    float fracJourney = currentTime / 0.1f;
    gameObject.transform.position = Vector3.Lerp(pointA.position, pointB.position, fracJourney);
    gameObject.transform.rotation = Quaternion.Lerp(pointA.rotation, pointB.rotation, fracJourney);
  }
}
