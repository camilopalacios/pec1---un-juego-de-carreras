﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RaceManager : MonoBehaviour
{
  public GameObject[] checkpoints;
  public GameObject currentCheckpoint;
  public CarPath carPath;

  public CarPathLoader carPathLoader;
  public TimeCount timeCount;
  public LapCount lapCount;
  public GhostController ghostController;
  public ReplayController replayController;

  public float countDownToStart = 3f;
  private WaitForSeconds startWait;

  public GameObject car, carReplay;
  public bool saved = false;
  public bool finished = false;
  public bool playingReplay = false;

  public UIController uiController;
  public AudioSource checkpointSound;

  void Awake()
  {
    LoadBestRaceData();
  }

  // Use this for initialization
  void Start()
  {
    startWait = new WaitForSeconds(countDownToStart);
    lapCount = new LapCount();
    StartCoroutine(RaceStarting());
    DeactivateAllCheckpoints();
    ActivateCheckpointByIndex(0);
    ghostController.PrepareData(carPath);
    carReplay.SetActive(false);
  }

  // Update is called once per frame
  void Update()
  {
    if (lapCount.GetCurrentLap() >= lapCount.GetMaxLaps())
    {
      if (!finished)
      {
        ShowTimeResult();
        finished = true;
      }
      if (!saved)
      {
        if (carPath == null)
        {
          uiController.ShowNewRecordMessage();
          car.GetComponent<CarPathRecorder>().SavePath();
#if UNITY_EDITOR
          UnityEditor.AssetDatabase.Refresh();
#endif
          saved = true;
        }
        else if (Time.timeSinceLevelLoad < carPath.GetTotalTime())
        {
          uiController.ShowNewRecordMessage();

          car.GetComponent<CarPathRecorder>().SavePath();
#if UNITY_EDITOR
          UnityEditor.AssetDatabase.Refresh();
#endif
          saved = true;
        }
        else
        {
          if(!playingReplay) uiController.ShowKeepTryingMessage();
        }
      }
      StopCar();
      StartCoroutine(RaceEnding());
    }
    UpdateCurrentLapTime();
  }

  private IEnumerator RaceStarting()
  {
    DisableCarControl();
    yield return startWait;
    EnableCarControl();
    timeCount.StartTimeCount();
  }

  private IEnumerator RaceEnding()
  {
    yield return new WaitForSeconds(4f);
    DisableCarControl();
    DisableCar();
    DisableGhost();
    ClearUI();
    playingReplay = true;
    carReplay.SetActive(true);
    replayController.PlayReplay(car.GetComponent<CarPathRecorder>().GetCarPath());
  }

  private void DisableCarControl()
  {
    car.GetComponent<UnityStandardAssets.Vehicles.Car.CarUserControl>().enabled = false;
  }

  private void EnableCarControl()
  {
    car.GetComponent<UnityStandardAssets.Vehicles.Car.CarUserControl>().enabled = true;
  }

  private void DisableCar(){
    car.SetActive(false);
  }

  private void DisableGhost(){
    ghostController.DisableGhost();
  }

  private void ClearUI(){
    uiController.DisplayReplay();
  }

  private void StopCar()
  {
    car.GetComponent<Rigidbody>().drag = 10;
    car.GetComponent<Rigidbody>().angularDrag = 10;
  }

  private void DeactivateAllCheckpoints()
  {
    foreach (GameObject checkpoint in checkpoints)
    {
      checkpoint.SetActive(false);
    }
  }

  private void ActivateCheckpointByIndex(int index)
  {
    currentCheckpoint = checkpoints[index];
    currentCheckpoint.SetActive(true);
  }

  public void CheckpointReached(GameObject checkpoint)
  {
    checkpointSound.Play();
    checkpoint.SetActive(false);
    for (int i = 0; i < checkpoints.Length; ++i)
    {
      if (checkpoints[i] == checkpoint)
      {
        if (i < checkpoints.Length - 1)
        {
          ActivateCheckpointByIndex(i + 1);
        }
        else if (i == checkpoints.Length - 1)
        { // End of lap
          ActivateCheckpointByIndex(0);
          UpdateLap();
        }
      }
    }
  }

  private void UpdateLap()
  {
    lapCount.SaveLap(timeCount.GetCurrentLapTimeFormatted());
    uiController.DisplayLapValue(lapCount.GetCurrentLap(), timeCount.GetCurrentLapTimeFormatted());
    uiController.UpdateLapText(lapCount.GetCurrentLapFormatted());
    timeCount.ResetLapTime();
  }

  private void UpdateCurrentLapTime()
  {
    uiController.UpdateLapTimeText(timeCount.GetCurrentLapTimeFormatted());
  }

  private void LoadBestRaceData()
  {
    carPathLoader = new CarPathLoader();
    carPath = carPathLoader.getCarPath();
  }

  private void ShowTimeResult()
  {
    int minutes = (int)Time.timeSinceLevelLoad / 60;
    float seconds = Time.timeSinceLevelLoad % 60;

    string result = "TOTAL TIME: ";
    if (minutes < 10) result += "0";
    result += minutes + ":";
    result += seconds.ToString("00.0");
    uiController.ShowTimeResult(result);
  }
}