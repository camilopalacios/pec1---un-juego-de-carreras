﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CarPathRecorder : MonoBehaviour
{
  public CarPath carPath;
  public bool saved = false;
	public string filePath;

  // Use this for initialization
  void Start()
  {
    carPath = new CarPath();
    InvokeRepeating("KeepCarTransform", 3f, 0.1f); // Save a sample every 0.1 seconds.
    filePath = Application.dataPath + "/Resources/BestRace.json";
  }

	public void SavePath(){
    carPath.SetTotalTime(Time.timeSinceLevelLoad);
		string journeyData = JsonUtility.ToJson(carPath);
    File.WriteAllText (filePath, journeyData);
    saved = true;
	}

  void KeepCarTransform()
  {
    carPath.SavePoint(gameObject.transform, Time.timeSinceLevelLoad);
  }

  public CarPath GetCarPath(){
    return carPath;
  }
}
