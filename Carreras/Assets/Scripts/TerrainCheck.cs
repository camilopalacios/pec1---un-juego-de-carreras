﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainCheck : MonoBehaviour
{
  void OnTriggerEnter(Collider other)
  {
    gameObject.GetComponent<Rigidbody>().drag = 0.1f;
  }
  void OnTriggerExit(Collider other)
  {
    gameObject.GetComponent<Rigidbody>().drag = 0.5f;
  }
}
