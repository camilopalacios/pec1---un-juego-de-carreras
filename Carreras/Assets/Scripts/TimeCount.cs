﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeCount : MonoBehaviour
{
  public float minutes, seconds, milliseconds;
  public bool countTime = false;

  public void StartTimeCount()
  {
    countTime = true;
    ResetLapTime();
  }

  // Update is called once per frame
  void Update()
  {
    UpdateTimeCount();
  }

  private void UpdateTimeCount()
  {
    if (countTime)
    {
      milliseconds += Time.deltaTime * 10;
      if (milliseconds >= 10)
      {
        milliseconds = 0f;
        seconds++;
      }
      if (seconds >= 60)
      {
        seconds = 0f;
        minutes++;
      }
    }
  }

  public string GetCurrentLapTimeFormatted()
  {
    string result = "";
    if(minutes < 10) result += "0";
    result += minutes + ":";
    if(seconds < 10) result += "0";
    result += seconds + ".";
    result += (int)milliseconds;
    return result;
  }

  public void ResetLapTime(){
    minutes = 0f;
    seconds = 0f;
    milliseconds = 0f;
  }
}
