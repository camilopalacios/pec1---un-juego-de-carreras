﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostTest : MonoBehaviour
{

  public Transform pointA, pointB;
  public float startTime, endTime=10f;
	public GameObject ghostCar;

  void Start()
  {
    startTime = 0;
  }

  void FixedUpdate()
  {
		float currentTime = Time.timeSinceLevelLoad - startTime;
		float fracJourney = currentTime / endTime;
		ghostCar.transform.position = Vector3.Lerp(pointA.position, pointB.position, fracJourney);
		ghostCar.transform.rotation  = Quaternion.Lerp(pointA.rotation, pointB.rotation, fracJourney);

		Debug.Log(Time.timeSinceLevelLoad);
  }
}
