﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CarPath
{
	public List<CarPoint> points = new List<CarPoint>();
	public float totalTime;

	public void SavePoint(Transform transf, float time){
		CarPoint newPoint = new CarPoint(transf.position,transf.rotation,time);
		points.Add(newPoint);
	}

	public void PrintPath(){
		foreach(CarPoint point in points){
			Debug.Log(point.ToString());
		}
	}

	public List<CarPoint> GetCarPoints(){
		return points;
	}

	public float GetTotalTime(){
		return totalTime;
	}

	public void SetTotalTime(float t){
		totalTime = t;
	}
}

[System.Serializable]
public class CarPoint {
	public Vector3 position;
  public Quaternion rotation;
  public float time;

	public CarPoint(Vector3 pos, Quaternion rot, float t){
		position = pos;
		rotation = rot;
		time = t;
	}

	public Vector3 GetPosition(){
		return position;
	}

	public Quaternion GetRotation(){
		return rotation;
	}

	public float GetTime(){
		return time;
	}

	public override string ToString(){
		return "| Position : " + position + " | Rotation: " + rotation + " | Time: " + time;
	}
}
