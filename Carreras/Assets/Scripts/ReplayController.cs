﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReplayController : MonoBehaviour
{
	public AudioSource source;
	public AudioClip music;
  public CarReplay car;
  private bool replaying = false;


  public void PlayReplay(CarPath carPath)
  {
    if (!replaying)
    {
      replaying = true;
      car.StartReplay(carPath);
			source.clip = music;
			source.loop = true;
			source.volume = 0.3f;
			source.Play();
    }
  }

	void Update(){
		if(replaying){
			bool exit = Input.GetButtonDown("Cancel");
			if(exit) SceneManager.LoadScene("Intro");
		}
	}
}
