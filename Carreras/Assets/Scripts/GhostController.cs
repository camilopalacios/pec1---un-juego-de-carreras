﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostController : MonoBehaviour
{
  public Transform pointA, pointB;
  public float startTime, endTime;
  public float currentTime;

  public List<CarPoint> points;
  public int currentSegmentIndex = 0;

  void Awake()
  {
    RestartValues();
  }

  void FixedUpdate()
  {
    UpdateSegment();
    MoveGhost();
  }

  private void RestartValues()
  {
    pointA.position = Vector3.zero;
    pointA.rotation = Quaternion.identity;
    pointB.position = Vector3.zero;
    pointB.rotation = Quaternion.identity;
    startTime = 0f;
    endTime = 0f;
    points = new List<CarPoint>();
  }

  public void PrepareData(CarPath carPath)
  {
    if (carPath != null)
    {
      points = carPath.GetCarPoints();
      if (points.Count >= 2) PrepareFirstSegment();
      else
      {
        DisableGhost();
      }
    }
    else
    {
      DisableGhost();
    }
  }

  public void DisableGhost(){
    gameObject.SetActive(false);
  }

  private void PrepareFirstSegment()
  {
    pointA.position = points[currentSegmentIndex].GetPosition();
    pointA.rotation = points[currentSegmentIndex].GetRotation();
    startTime = points[currentSegmentIndex].GetTime();
    currentSegmentIndex++;
    pointB.position = points[currentSegmentIndex].GetPosition();
    pointB.rotation = points[currentSegmentIndex].GetRotation();
    endTime = points[currentSegmentIndex].GetTime();
    currentSegmentIndex++;
  }

  private void UpdateSegment()
  {
    if (Time.timeSinceLevelLoad >= endTime)
    {
      // Point A data
      pointA.position = pointB.position;
      pointA.rotation = pointB.rotation;
      startTime = endTime;
      // Point B data
      if (points.Count > currentSegmentIndex)
      {
        pointB.position = points[currentSegmentIndex].GetPosition();
        pointB.rotation = points[currentSegmentIndex].GetRotation();
        endTime = points[currentSegmentIndex].GetTime();
        currentSegmentIndex++;
      }
    }
  }

  private void MoveGhost()
  {
    float currentTime = Time.timeSinceLevelLoad - startTime;
    float fracJourney = currentTime / (endTime - startTime);
    gameObject.transform.position = Vector3.Lerp(pointA.position, pointB.position, fracJourney);
    gameObject.transform.rotation = Quaternion.Lerp(pointA.rotation, pointB.rotation, fracJourney);
  }
}
