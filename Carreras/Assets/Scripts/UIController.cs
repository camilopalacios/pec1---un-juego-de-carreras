﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {
	public GameObject statsPanel, resultPanel;
	public Text lap, lapTime, lap1, lap2, lap3;
	public Text timeResult, countDownToStart, replayMessage;
	public Image endMessage;
	public Sprite newRecord, keepTrying;

	void Awake(){
		resultPanel.SetActive(false);
		endMessage.enabled = false;
		replayMessage.enabled = false;
	}

	void Update(){
		if(Time.timeSinceLevelLoad > 4) countDownToStart.enabled = false;
		else if(Time.timeSinceLevelLoad > 3)countDownToStart.text = "GO";
		else if(Time.timeSinceLevelLoad > 2)countDownToStart.text = "1";
		else if(Time.timeSinceLevelLoad > 1)countDownToStart.text = "2";
	}

	public void UpdateLapTimeText(string value){
		lapTime.text = value;
	}

	public void UpdateLapText(string value){
		lap.text = value;
	}

	public void DisplayLapValue(int lap, string value){
		switch(lap){
			case 1:
				lap1.text = value;
				break;
			case 2:
				lap2.text = value;
				break;
			case 3:
				lap3.text = value;
				break;
		}
	}

	public void ShowTimeResult(string value){
		timeResult.text = value;
		resultPanel.SetActive(true);
		statsPanel.SetActive(false);
	}

	public void ShowNewRecordMessage(){
		endMessage.enabled = true;
		endMessage.sprite = newRecord;
	}

	public void ShowKeepTryingMessage(){
		endMessage.enabled = true;
		endMessage.sprite = keepTrying;
	}

	public void DisplayReplay(){
		statsPanel.SetActive(false);
		resultPanel.SetActive(false);
		endMessage.enabled = false;
		countDownToStart.enabled = false;

		replayMessage.enabled = true;
	}
}
